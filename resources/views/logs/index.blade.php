@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12 m12">
          
          <!-- Menú botón flotante -->
          <div class="fixed-action-btn">
                <a class="btn-floating btn-large waves-effect cyan waves-light modal-trigger" href="{{url('/inicio')}}"><i class="material-icons">home</i></a>
            </div>

            <h2 style="text-align: center; color: #8E24AA;">Reporte de Usuarios</h2>
            @if (isset($status))
                <div class="alert alert-success">
                    {{ $status }}
                </div>
            @endif

            <!--Materialize-->
            <div class="row">
                <table class="striped highlight">
                    <thead>
                      <tr>
                          <th>Universo de Usuarios</th>
                          <th>Total Super usuarios</th>
                          <th>Total Administradores</th>
                          <th>Total de Usuarios</th>
                          <th>Total inicios de sesión</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>{{$data['universo']}}</td>                            
                        <td>{{$data['super']}}</td>
                        <td>{{$data['admin']}}</td>
                        <td>{{$data['normal']}}</td>
                        <td>{{$data['login']}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
