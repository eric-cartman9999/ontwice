@extends('layouts.app')

@section('content')
<div class="container-fluid" style="padding: 0;">
    <div class="row">
        <div class="col-sm-12 col-md-6 bg_login valign-wrapper"></div>
        <div class="col-sm-12 col-md-6" style="padding: 0;">
            <ul id="tabs-swipe-demo" class="tabs">
                <li class="tab col s3 m6 l6"><a class="active cyan-text" href="#test-swipe-1">Iniciar Sesión</a></li>
                <li class="tab col s3 m6 l6"><a class="cyan-text" href="#test-swipe-2">Registrarse</a></li>
            </ul>
            <div id="test-swipe-1" class="row white" style="padding: 10%; height: 94vh;">
                    <div class="col s12 text-center"><h1><strong>Ontwice</strong></h1></div>
                    <form class="form-horizontal col s12" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-field col s12 m12">
                              <i class="material-icons prefix">mail_outline</i>
                              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                              <label for="icon_prefix">Correo Electrónico</label>
                            </div>

                            <!--<label for="email" class="col-md-4 control-label">Correo Electrónico: </label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>-->
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-field col s12 m12">
                              <i class="material-icons prefix">lock_outline</i>
                              <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                              <label for="icon_prefix">Contraseña</label>
                            </div>

                            <!--<label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>-->
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary waves-effect waves-light cyan">
                                    Iniciar sesión
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} style="opacity: 1;"> Recuérdame
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <a class="btn-link" href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
            <div id="test-swipe-2" class="row white" style="padding: 10%; height: 94vh;">
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="input-field col s12">
                          <i class="material-icons prefix">perm_identity</i>
                          <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                          @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                          @endif
                          <label for="icon_prefix">Nombre (s)</label>
                        </div>

                        <!--<label for="name" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>-->
                    </div>


                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-field col s12">
                          <i class="material-icons prefix">mail_outline</i>
                          <input id="email2" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                          <label for="icon_prefix">Correo Electrónico</label>
                        </div>

                        <!--<label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email2" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>-->
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-field col s12">
                          <i class="material-icons prefix">lock_outline</i>
                          <input id="password2" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                          <label for="icon_prefix">Contraseña</label>
                        </div>

                        <!--<label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password2" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>-->
                    </div>

                    <div class="form-group">
                        <div class="input-field col s12">
                          <i class="material-icons prefix">lock_outline</i>
                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                          <label for="icon_prefix">Confirmar Contraseña</label>
                        </div>

                        <!--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>-->
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect waves-light cyan">
                                Registrarse
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        
            <!--<div class="panel panel-default">
                <div class="panel-heading cyan lighten-5">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo Electrónico: </label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuérdame
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Iniciar sesión
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    ¿Olvidaste tu contraseña?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>-->
        </div>
    </div>
</div>
@endsection
