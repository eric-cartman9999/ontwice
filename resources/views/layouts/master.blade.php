<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('master.name', 'Ontwice') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"  media="screen,projection"/>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <style>
        #menuPrueba{
            list-style-type: none;
            margin: 0;
            padding: 0;
            font-size: 1.2em;
            /*overflow: hidden;*/
            /*background-color: #00BCD4;*/
        }

        #menuPrueba li {
            float: left;
        }

        #menuPrueba li a {
            display: inline-block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        #menuPrueba li a:hover {
            background-color: rgba(0,188,212,0.5);
            color: #fff!important;
        }

        #menuPrueba .active {
            background-color: rgba(0,188,212,0.1);
            color: #fff;
        }
        .container-fluid{
            padding-left: 0;
            padding-right: 0; 
        }
        .navbar{
            margin-bottom: 0;
        }
        .navbar-default .navbar-toggle{
            border-color: transparent;
        }
        .navbar-toggle{
            margin:0;
            padding:0;
        }
        .hoverico{
            color: #78909c;
        }
        .hoverico:hover{
            color: #fff;
        }

        li.dropmenu{
            width: 100%;
        }
        li.dropmenu:hover{
            /*background-color: rgba(0,188,212,0.5);*/
        }
        a.opcionesMenu2{
            width: 100%;
        }
        .dropdown-menu{
            padding: 0;
        }
         /* label focus color */
        .input-field input[type=text]:focus + label {
             color: #000;
        }
        /* label underline focus color */
       .input-field input[type=text]:focus {
         border-bottom: 1px solid #000;
         box-shadow: 0 1px 0 0 #000;
       }

    input:not([type]):focus:not([readonly]), input[type=text]:not(.browser-default):focus:not([readonly]), input[type=password]:not(.browser-default):focus:not([readonly]), input[type=email]:not(.browser-default):focus:not([readonly]), input[type=url]:not(.browser-default):focus:not([readonly]), input[type=time]:not(.browser-default):focus:not([readonly]), input[type=date]:not(.browser-default):focus:not([readonly]), input[type=datetime]:not(.browser-default):focus:not([readonly]), input[type=datetime-local]:not(.browser-default):focus:not([readonly]), input[type=tel]:not(.browser-default):focus:not([readonly]), input[type=number]:not(.browser-default):focus:not([readonly]), input[type=search]:not(.browser-default):focus:not([readonly]), textarea.materialize-textarea:focus:not([readonly]){
        border-bottom: 1px solid #7b1fa2;
        box-shadow: 0 1px 0 0 #7b1fa2;
    }
    input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea{
        border-bottom: 1px solid #7b1fa2;
    }
    .tabs .tabpurple{
        text-transform: none;
    }
    .tabs .tabpurple a:hover{
        text-decoration: none;
    }
    .tabs .indicator{

    }
    .tabs .indicator {
        background-color: #7b1fa2;
    }
    .form-control{
        border: 1px solid #7b1fa2;
        background-color: transparent;
    }
    ul li.tabpurple{
        height: 60px !important; 
        padding: 10px 0 !important; 
        line-height: 20px !important;
    }
    ul li.tabpurple a{
        color: rgba(142,36,170,0.7) !important;
        text-decoration: none;
    }
    .input-field .prefix.active{
        color: #7b1fa2;
    }
     .input-field input:focus + label {
       color: #7b1fa2 !important;
     }
     .padbtn{
        margin-top: 2%;
     }
     .btn:hover, .btn:active{
        color: #fff;
    }
    </style>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top cyan">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <i class="material-icons">menu</i>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/inicio') }}" style="color: white;">
                        {{ config('master.name', 'Ontwice') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                           <li><a href="{{ route('login') }}" style="color: white;">Login</a></li>
                            <li><a href="{{ route('register') }}" style="color: white;">Register</a></li>
                        @else
                            <li class="dropdown" >
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" style="color: #fff; background-color: transparent;">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}" class="cyan-text" style="background-color: transparent;" 
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>         
            </div>
        </nav>
        @yield('content')
    </div>

    @yield('js-partials')
</body>
</html>
