@extends('layouts.master')
<style>
    .azul{
        background-color: blue;
    }
</style>
@section('content')

<div class="container">
    <div class="row">
        <div class="col s12 m12">

            @if (\Session::has('status'))
                <input type="hidden" id="mensaje" value="{{\Session::get('status') }}">
               <script>
                    var valor = $("#mensaje").val();
                    Materialize.toast(valor, 7000, 'red')
                </script>
            @endif

            <div class="fixed-action-btn">
                <a class="btn-floating btn-large waves-effect cyan waves-light modal-trigger" href="{{url('/inicio')}}"><i class="material-icons">home</i></a>
            </div>

            <h3>Editar usuario:</h3>
                @if (isset($status))
                    <div class="alert alert-success">
                        {{ $status }}
                    </div>
                @endif

                <form method="POST" action="{{url("/actualiza-usuario")}}" enctype="multipart/form-data">
                    
                    {{csrf_field()}}

                    <input type="hidden" name="user_id" value="{{$user->id}}">


                    <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                        <div class="form-grop">
                            <label for="nombre">Nombre:</label>

                            <input type="text" class="form-control" name="nombre"  id="nombre" value="{{ $user->name }}" required></input>
                            
                            @if ($errors->has('nombre'))
                            <span class="mdl-textfield__error">{{ $errors->first('nombre') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="form-grop">
                            <label for="email">Email:</label>

                            <input type="email" class="form-control" name="email"  id="email" value="{{ $user->email }}" required></input>
                            
                            @if ($errors->has('email'))
                            <span class="mdl-textfield__error">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group" style="<?php if(Auth()->user()->rol_id == 3){ echo 'display: none;';} ?>">
                      <label for="resumen">Rol:</label>
                        <select name="rol_id">
                            @foreach($roles as $rol)
                                <option value="{{$rol->id}}" {{($user->rol_id == $rol->id) ? 'selected' : ''}}>{{$rol->rol}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary purple darken-1 waves-effect waves-light">Editar</button>
                    </div>
                </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('select').material_select();
    });        
</script>
@endsection
