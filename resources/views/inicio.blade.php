@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12 m12">
            <!-- Toast para enviar mensajes al usuario -->
              @if (\Session::has('status'))
                  <input type="hidden" id="mensaje" value="{{\Session::get('status') }}">
                 <script>
                      var valor = $("#mensaje").val();
                      Materialize.toast(valor, 7000, 'green')
                  </script>
              @endif

              <div class="fixed-action-btn">
                <a class="btn-floating btn-large green">
                  <i class="large material-icons">add</i>
                </a>
                <ul>
                  @if(Auth()->user()->rol_id == 1)
                    <li><a href="{{url('/dashboard')}}" class="btn-floating orange darken-1"><i class="material-icons">insert_chart</i></a></li>
                  @endif
                  <li><a href="{{url('edicion-usuario/' . Auth()->user()->id)}}" class="btn-floating blue"><i class="material-icons">account_circle</i></a></li>
                </ul>
              </div>

            <h2 style="text-align: center; color: #8E24AA;">Lista de Usuarios</h2>
            @if (isset($status))
                <div class="alert alert-success">
                    {{ $status }}
                </div>
            @endif

            <!--Materialize-->
            <div class="row">
                <table class="striped highlight">
                    <thead>
                      <tr>
                          <th>Nombre</th>
                          <th>Correo Electrónico</th>
                          <th>Rol</th>
                          <th>Fecha de Creación</th>
                          @if(Auth()->user()->rol_id != 3)
                            <th>Acciones</th>
                          @endif
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->rol}}</td>
                            <td>{{$user->created_at}}</td>
                            @if(Auth()->user()->rol_id != 3)
                              <td>
                                  @if($user->rol_id != 1)
                                      <a href="{{url('edicion-usuario/' . $user->id)}}" class="waves-effect waves-light btn">Actualizar</a>
                                      <a href="{{url('elimina-usuario/' . $user->id)}}" class="waves- effect waves-light btn blue">Eliminar</a>
                                  @endif
                              </td>
                            @endif
                        </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>
@endsection
