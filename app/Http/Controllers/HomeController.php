<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // Obtiene a los usuarios registrados en el sistema
        $users =  User::join('roles', 'users.rol_id', '=', 'roles.id')
                    ->select('users.id', 'users.id', 'users.name', 'users.email', 'users.rol_id', 'users.created_at', 'roles.rol')
                    ->get();

        return view('inicio')->with(compact('users'));
    }
}
