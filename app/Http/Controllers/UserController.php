<?php

namespace App\Http\Controllers;

use App\Logs;
use App\Rol;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 1) Verifica que el usuario exista
        $user = User::find($id);

        if (!$user) {
            return redirect()->back();
        }

        // 2) Obtiene los roles
        $roles = Rol::all();

        return view('usuarios.edit')->with(compact('user'))
                                    ->with(compact('roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'nombre' => 'required|string',
            'email' => 'required|email',
            'rol_id' => 'required|exists:roles,id'
        ]);

        if ($validator->fails()) {
            \Log::info('Error actualizar usuario:');
            \Log::info($validator->errors());
            
            \Session::flash('status', "Datos incorrectos.");

            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        // Obtener el usuario a actualizar
        $user = User::find($request->user_id);
        $user->name = $request->nombre;
        $user->email = $request->email;
        $user->rol_id = $request->rol_id;
        $user->save();

        return redirect(url('/inicio'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // 1) Verifica que el usuario exista
        $user = User::find($id);

        if (!$user) {
            return redirect()->back();
        }

        // Validación para que no permita eliminar a un super usuario
        if ($user->rol_id == 1) {
            \Session::flash('status', "No puedes eliminar a un super usuario.");

            return redirect()->back();    
        }

        // 2) Elimina logs del usuario
        $logs = Logs::where('user_id', $id)->delete();

        // 3) Elimina usuario
        $user->delete();

        // 4) Añade variable de sesión
        \Session::flash('status', "Usuario eliminado correctamente.");

        return redirect()->back();
    }
}
