<?php

namespace App\Http\Controllers;

use App\Logs;
use App\User;

use Illuminate\Http\Request;

class LogsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$data['universo'] = User::count();
    	$data['super'] = User::rol(1)->count();
    	$data['admin'] = User::rol(2)->count();
    	$data['normal'] = User::rol(3)->count();
    	$data['login'] = Logs::where('description', 'LIKE', 'Inicio de sesión')->count();

    	return view('logs.index')->with(compact('data'));
    }
}
