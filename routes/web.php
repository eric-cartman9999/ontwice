<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@index');
Route::get('/home', 'Auth\LoginController@index');

Auth::routes();

//Página principal
Route::get('/inicio', 'HomeController@index')->name('inicio');

/**
 * Usuario
 */
Route::get('/edicion-usuario/{id}', 'UserController@edit');
Route::post('/actualiza-usuario', 'UserController@update');
Route::get('/elimina-usuario/{id}', 'UserController@destroy');

/**
 * Dashboard
 */
Route::get('/dashboard', 'LogsController@index');