<h1>Prueba técnica Ontwice</h1>
<h3>Eric Montes de Oca Juárez</h3>

<p>El siguiente sistema es un pequño ABC (Altas, Bajas y Cambios). Los requerimientos con los que fue desarrollado este proyecto son: </p>
<br>
<ul>
	<li>PHP 7.1</li>
	<li>Laravel 5.6</li>
	<li>MariaDB 10.1.35 </li>
	<li>Materialize</li>
	<li>HTML5</li>
	<li>CSS</li>
	<li>XAMPP</li>
</ul>
<br>
<p>En el enlace que aparece a continuación se puede encontrar una breve documentación para entender mejor el funcionamiento de esta prueba:</p>
<br>
<a href="https://docs.google.com/document/d/1T9WCCw2XnkqU3GYl8-5ARyEfpwyqLkiu7HQoBS0ttR8/edit?usp=sharing">Documentación</a>