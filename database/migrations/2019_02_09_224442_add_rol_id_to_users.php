<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRolIdToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!(Schema::hasColumn('users', 'rol_id'))){
            Schema::table('users', function($table){
                $table->integer('rol_id')->unsigned()->default(3)->after('email');
                $table->foreign('rol_id')
                        ->references('id')
                        ->on('roles')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropForeign('users_rol_id_foreign');
            $table->dropColumn('rol_id');
        });
    }
}
