<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
	            'name' => 'Cuenta de Super Usuario',
	            'email' => 'super@ontwice.com',
	            'password' => bcrypt('Super4dm1n#8'),
	            'rol_id' => 1,
	            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
	            'name' => 'Eric Montes de Oca Juarez',
	            'email' => 'eric.mdoj8@gmail.com',
	            'password' => bcrypt('Juarez#8'),
	            'rol_id' => 2,
	            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
	            'name' => 'Luis Perez',
	            'email' => 'luis@perez.com',
	            'password' => bcrypt('usuario_normal'),
	            'rol_id' => 3,
	            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
	            'name' => 'Fernanda Campos',
	            'email' => 'fer@campos.com',
	            'password' => bcrypt('usuario_normal2'),
	            'rol_id' => 3,
	            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        	],
        ]);
    }
}
